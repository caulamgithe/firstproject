import React from 'react';
import {Component} from 'react';
import styles from "../css/style";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {IData} from "../redux/saga/turnOnAppSaga";

interface Props {
    data: IData,
}


export default class OneGridData extends Component<Props> {

    constructor(props: Props){
        super(props);
    }

    pressedOnEachElement(value: string) {
        alert(value);
    }

    render() {
        return (
            <TouchableOpacity onPress={() => {
                this.pressedOnEachElement(this.props.data.data.title)
            }}>
                <View style={styles.eachGrid}>
                    <Image style={{flex: 1}} source={{uri: this.props.data.data.imageURL}}/>
                    <View style={styles.contentGrid}>
                        <Text style={styles.textContentGrid}>
                            {this.props.data.data.title}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}