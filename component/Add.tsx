import React from 'react';
import {Component} from 'react';
import {View, TextInput, Button} from 'react-native';
import firebase from '../api/api';

interface Props{
}

interface State {
    imageUrl: string,
    title: string,
    content: string,
}

export default class Add extends Component<Props, State> {

    constructor(props: Props, states: State) {
        super(props);
        this.state = {
            imageUrl: states.imageUrl||'',
            title: states.title||'',
            content: states.content||'',
        };
    }


    // push a new news into database
    pressedAddButton() {
        firebase.database().ref("News").push({
            imageURL: this.state.imageUrl,
            title: this.state.title,
            content: this.state.content
        }, () => {
            alert('success');
            this.setState({
                imageUrl: '',
                title: '',
                content: '',
            });
        })
            .catch((error: any) => {
                alert('failed, reason: ' + error.message)
            });
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <TextInput placeholder={'Enter image URL'} onChangeText={(value) => {
                    this.setState({imageUrl: value})
                }}/>
                <TextInput placeholder={'Enter Title'} onChangeText={(value) => {
                    this.setState({title: value})
                }}/>
                <TextInput placeholder={'Enter Content'} onChangeText={(value) => {
                    this.setState({content: value})
                }}/>
                <Button title={'Add'} onPress={() => {
                    this.pressedAddButton()
                }}/>
            </View>
        );
    };
}