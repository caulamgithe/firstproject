import React from 'react';
import {Component} from 'react';
import styles from "../css/style";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {IData} from "../redux/saga/turnOnAppSaga";

interface Props {
    data: IData,
}

export default class OneRowData extends Component<Props> {

    constructor(props: Props){
        super(props);
    }

    pressedOnEachElement(value: string) {
        alert(value);
    }

    render() {
        return (
            <TouchableOpacity onPress={() => {
                this.pressedOnEachElement(this.props.data.data.title)
            }}>
                <View style={styles.eachRow}>
                    <View style={styles.imageContainerRow}>
                        <Image style={styles.imageRow} source={{uri: this.props.data.data.imageURL}}/>
                    </View>
                    <View style={styles.rightContentRow}>
                        <Text style={styles.titleRow}>
                            {this.props.data.data.title}
                        </Text>
                        <Text style={styles.contentRow}>
                            {this.props.data.data.content}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}