import React, {ReactElement} from 'react';
import {Component} from 'react';
import {View, ListView, Button, ActivityIndicator} from 'react-native';
import Controller from "./ControllerBar";
import OneRowData from "./OneRowData";
import OneGridData from "./OneGridData";
import {connect} from 'react-redux';
import {updateListNoteFromServer} from '../redux/action/action';
import {IData} from '../redux/saga/turnOnAppSaga';
import {IState} from "../redux/store/store";

interface Props extends IState {
    updateListNoteFromServer: typeof updateListNoteFromServer,
    navigation?: any,
}

class Home extends Component<Props> {


    constructor(props: Props) {
        super(props);
    }

    // get data from database, and refresh data if the data changed
    componentWillMount() {
        this.props.updateListNoteFromServer();
    }

    // one data in listview or gridview
    oneData(rowData: IData): any {
        if (this.props.selectedPickerValue === 'row') {
            return (
                <OneRowData data={rowData}/>
            );
        } else if (this.props.selectedPickerValue === 'grid') {
            return (
                <OneGridData data={rowData}/>
            );
        }
    }

    // the controller bar
    renderController() {
        return (
            <Controller/>
        );
    }

    //the listview
    renderListView() {
        if (this.props.isLoading === true) {
            return (
                <View style={{flex: 1}}>
                    <ActivityIndicator size={'large'}/>
                </View>
            );
        } else {
            if (this.props.selectedPickerValue === 'row') {
                return (
                    <ListView
                        dataSource={this.props.dataSource}
                        renderRow={(rowData) => {
                            return this.oneData(rowData)
                        }}
                    />
                );
            } else if (this.props.selectedPickerValue === 'grid') {
                return (
                    <ListView
                        dataSource={this.props.dataSource}
                        renderRow={(rowData) => {
                            return this.oneData(rowData)
                        }}
                        contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
                        pageSize={this.props.dataList.length}
                    />
                );
            }
        }
    }

    // add screen to add a news
    pressedAddButton() {
        this.props.navigation.push('AddScreen');
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1, backgroundColor: 'green'}}>{this.renderController()}</View>
                <View style={{flex: 11}}>{this.renderListView()}</View>
                <View style={{flex: 1, backgroundColor: 'green'}}>
                    <Button title={'Add'} onPress={() => {
                        this.pressedAddButton()
                    }}/>
                </View>
            </View>
        );
    }
}

export default connect((state: IState) => ({
    isLoading: state.isLoading,
    dataList: state.dataList,
    dataSource: state.dataSource,
    selectedPickerValue: state.selectedPickerValue
}), {
    updateListNoteFromServer,
})(Home);
