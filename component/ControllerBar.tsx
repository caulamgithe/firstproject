import React from 'react';
import {Component} from 'react';
import styles from "../css/style";
import {Picker, TextInput, View} from "react-native";
import {connect} from 'react-redux';
import {changeTypeOfView, searchWithEmptyString, searchWithNonEmptyString} from '../redux/action/action';
import {IData} from "../redux/saga/turnOnAppSaga";


interface Props extends State{
    changeTypeOfView: typeof changeTypeOfView,
    searchWithEmptyString: typeof searchWithEmptyString,
    searchWithNonEmptyString: typeof searchWithNonEmptyString,
}

interface State {
    dataList: IData[],
    selectedPickerValue: string,
    searchValue: string,
}

class Controller extends Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    // change the perspective
    pickerSelectedValueChanged(value: string) {
        this.props.changeTypeOfView(value);
    }

    // search for news
    updateListBySearch(value: string) {
        if (value.trim() === '') {
            this.props.searchWithEmptyString(this.props.dataList);
        } else {
            this.props.searchWithNonEmptyString(value, this.props.dataList);
        }
    }

    render() {
        return (
            <View style={styles.controllerView}>
                <View style={{flex: 1}}>
                    <Picker
                        style={{flex: 1}}
                        onValueChange={(value) => {
                            this.pickerSelectedValueChanged(value);
                        }}
                        selectedValue={this.props.selectedPickerValue}>
                        <Picker.Item key={'row'} value={'row'} label={'row'}/>
                        <Picker.Item key={'grid'} value={'grid'} label={'grid'}/>
                    </Picker>
                </View>
                <View style={{flex: 5}}>
                    <TextInput
                        value={this.props.searchValue}
                        style={{flex: 1}}
                        placeholder={'Search'}
                        secureTextEntry={false}
                        onChangeText={(value) => {
                            this.updateListBySearch(value)
                        }}
                    />
                </View>
            </View>
        );
    }
}

export default connect((state: State) => {
    return {
        dataList: state.dataList,
        selectedPickerValue: state.selectedPickerValue,
        searchValue: state.searchValue
    };
},{
    changeTypeOfView,
    searchWithEmptyString,
    searchWithNonEmptyString
})(Controller);
