import {
    createAppContainer,
    createStackNavigator,
    NavigationRouteConfigMap,
    StackNavigatorConfig
} from "react-navigation";
import Home from '../component/Home';
import Add from '../component/Add';

const iStackConfig: StackNavigatorConfig = {
    initialRouteName: 'HomeScreen',
}

const iComponents: NavigationRouteConfigMap = {
    HomeScreen: Home,
    AddScreen: Add,
}

// create a stack navigator
var stacks = createStackNavigator(iComponents,iStackConfig);

export const AppContainer = createAppContainer(stacks);