import {createStore, applyMiddleware} from "redux";
import {reducer} from '../reducer/reducer';
import {ListView, ListViewDataSource} from "react-native";
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../saga/rootSaga';
import {IData} from "../saga/turnOnAppSaga";


export interface IState {
    isLoading: boolean,
    dataList: IData[],
    seachValue: string,
    dataSource: ListViewDataSource,
    selectedPickerValue: string,
}

export const defaultState: IState = {
    isLoading: false,
    dataList: [],
    seachValue: '',
    dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
    selectedPickerValue: 'row',
};

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);