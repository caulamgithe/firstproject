import {takeEvery, call} from 'redux-saga/effects';
import {news} from "../../api/api";
import {put} from 'redux-saga/effects';

export interface IData {
    key: string,
    data: IDataInData,
}

export interface IDataInData {
    content: string,
    title: string,
    imageURL: string,
}

interface IEachData {
    key: string,
    val(): IDataInData;
}

let data: IData[] = [];


function* updateListNoteFromServer() {
    try {
        yield call(() => {
            return new Promise((resolve, reject) => {
                news.on('value', (snap: any) => {
                    data = [];
                    snap.forEach((eachData: IEachData) => {
                        data.push({
                            key: eachData.key,
                            data: eachData.val(),
                        });
                    });
                    resolve(data);
                });
            });
        });
        yield put({
            type: 'updateListNoteFromServerSuccess',
            data: data,
        });
    } catch (error) {
        alert('' + error);
        yield put({
            type: 'updateListNoteFromServerFailed',
            data: [],
        });
        return;
    }
}

export function* watchTurnOnApp() {
    yield takeEvery('updateListNoteFromServer', updateListNoteFromServer);
}