import {defaultState, IState} from '../store/store';
import {IData} from "../saga/turnOnAppSaga";
import {IAction} from "../action/action";

export const reducer = (state: IState = defaultState, action: IAction) => {
    let defaultDataSource = defaultState.dataSource;
    switch (action.type) {
        case 'updateListNoteFromServerSuccess': {
            return {
                ...state,
                dataSource: defaultDataSource.cloneWithRows(action.data!),
                dataList: action.data!,
                isLoading: false,
            };
        }
        case 'updateListNoteFromServer': {
            return {
                ...state,
                isLoading: true,
            };
        }
        case 'updateListNoteFromServerFailed': {
            return {
                ...state,
                dataSource: defaultDataSource.cloneWithRows([]),
                dataList: action.data!,
                isLoading: false,
            };
        }
        case 'changeTypeOfView': {
            return {
                ...state,
                selectedPickerValue: action.value!,
            };
        }
        case 'searchWithEmptyString': {
            return {
                ...state,
                dataSource: defaultDataSource.cloneWithRows(action.searchedData!),
            };
        }
        case 'searchWithNonEmptyString': {
            let searchDataResults = action.searchFrom!.filter((eachData: IData) => {
                if (eachData.data.title.toLowerCase().includes(action.searchedValue!.toLowerCase())) {
                    return eachData;
                }
            });
            return {
                ...state,
                dataSource: defaultDataSource.cloneWithRows(searchDataResults),
            };
        }
        default: {
            return state;
        }
    }
}