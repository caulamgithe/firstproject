import {IData} from "../saga/turnOnAppSaga";


export interface IAction {
    type: string,
    value?: string,
    isLoading?: boolean,
    searchedData?: IData[],
    searchedValue?: string,
    searchFrom?: IData[],
    data?: IData[],
}

export function updateListNoteFromServer() {
    return {
        type: 'updateListNoteFromServer',
        isLoading: true,
    };
}

export function changeTypeOfView(value: string) {
    return {
        type: 'changeTypeOfView',
        value,
    };
}

export function searchWithEmptyString(data: IData[]) {
    return {
        type: 'searchWithEmptyString',
        searchedData: data,
    };
}

export function searchWithNonEmptyString(value: string, data: IData[]) {
    return {
        type: 'searchWithNonEmptyString',
        searchedValue: value,
        searchFrom: data,
    };
}